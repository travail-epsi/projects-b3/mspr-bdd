from collections import Counter
from dataclasses import replace
import os


def parser(filePath):
    string = ''
    with open("{}.ctl".format(filePath), encoding='utf-8') as c:
        raw_headers: list = c.readlines()[7:]
        cleaned_headers = []
        for header in raw_headers:
            cleaned_headers.append(header.replace('\n', '').replace('\t', '').replace(',', ';').replace(
                '(', '').replace(')', '').replace('"seq_tournee.nextval"', '').replace('"seq_demande.nextval"', '').replace('"seq_centre.nextval"', '').replace('trailing nullcols', '').replace('"seq_employe.nextval"', '').replace('"seq_typedechet.nextval"', '').strip())
        string = ''.join(map(str, cleaned_headers))
    c.close()

    lines = []
    with open("{}.txt".format(filePath), 'r+', encoding='ISO-8859-1') as cw:
        lines = cw.readlines()
    cw.close()

    with open("{}.csv".format(filePath), 'w+') as cwvsv:
        cwvsv.seek(0)
        cwvsv.write(string+'\n')
        for line in lines:
            cwvsv.write(line)
    cwvsv.close()


if __name__ == "__main__":
    path = os.path.dirname(os.path.abspath(__file__))
    dirlist = [item for item in os.listdir(
        path=path) if os.path.isdir(os.path.join(path, item))]
    file_list = []
    for dir in dirlist:
        file_list = [fl.replace('.txt', '.ctl')
                     for fl in os.listdir(os.path.join(path, dir))]
        counts = Counter(file_list)
        file_list = list(dict.fromkeys([file for file in file_list if counts[file]
                                        > 1]))

        for file in file_list:
            parser(os.path.join(path, dir, file.replace(".ctl", "")))
