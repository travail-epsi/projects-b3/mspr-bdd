import os
import pandas as pd
from collections import Counter

import blueprint.site as Site


binder = {"centre.csv": Site.Site()}


if __name__ == "__main__":
    path = os.path.dirname(os.path.abspath(__file__))
    dirlist = [item for item in os.listdir(
        path=path) if os.path.isdir(os.path.join(path, item))]
    for dir in dirlist:
        file_list = [fl
                     for fl in os.listdir(os.path.join(path, dir)) if '.csv' in fl]
        counts = Counter(file_list)
        for file in file_list:
            if binder.get(file) is not None:
                df = binder.get(file)
                content = pd.read_csv(os.path.join(path, dir, file), sep=';')
                for i in range(len(content)):
                    df.fill(content.iloc[i])
    for key, value in binder.items():
        value.dataframe.to_csv(os.path.join(
            path, "outputs", key), sep=';', index_label='id')
