import pandas as pd

class Site:
    dataframe = None

    def __init__(self):
        header = ["adresse", "cp", "id_ville"]
        self.dataframe = pd.DataFrame(columns=header)

    def fill(self, initialData):
        address = str(initialData['NORUECENTRE']) + \
            ' ' + str(initialData['RUECENTRE'])
        postalCode = initialData['CPOSTALCENTRE']
        newRow = {"adresse": address,
                  "cp": postalCode, "id_ville": 1}
        newDf = pd.DataFrame([newRow])
        self.dataframe = pd.concat([self.dataframe, newDf], ignore_index=True)
