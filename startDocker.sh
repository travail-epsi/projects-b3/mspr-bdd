OLD_CONTAINER_ID=$(docker ps -aqf "name=python_mspr")
WORKDIR="$(pwd)/Workspace"
IMAGE_NAME=python_mspr

docker build -t $IMAGE_NAME Docker/
echo $OLD_CONTAINER_ID
if [ -n "$OLD_CONTAINER_ID" ]; then
  docker rm -f $OLD_CONTAINER_ID
fi
docker run -d --name python_mspr \
  -v $WORKDIR:/usr/local/mspr \
  $IMAGE_NAME:latest

NEW_CONTAINER_ID=$(docker ps -aqf "name=python_mspr")
docker exec -it "$NEW_CONTAINER_ID" bash
exit 0
